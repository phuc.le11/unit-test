class Stack {
  constructor() {
    this.data = [];
    this.length = 0;
  }

  push(element) {
    this.data[this.length++] = element;
    return this.data[this.length - 1];
  }

  getLength() {
    return this.length;
  }

  isEmpty() {
    return this.length === 0;
  }

  peek() {
    if (this.isEmpty()) throw new Error('Stack is empty');
    return this.data[this.length - 1];
  }

  pop() {
    if (this.isEmpty()) throw new Error('Stack is empty');
    const value = this.peek();
    delete this.data[--this.length];
    return value;
  }

  clear() {
    this.length = 0;
    this.data = [];
  }

  print() {
    console.log(this.data.toString());
  }
}

module.exports = new Stack();
