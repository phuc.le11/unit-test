const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test clear() when stack is not empty', () => {
  for (let i = 0; i < 5; i++) {
    stack.push(i);
  }
  stack.clear();
  expect(stack.isEmpty()).toBe(true);
});

test('Test clear() when stack is empty', () => {
  stack.clear();
  expect(stack.isEmpty()).toBe(true);
});
