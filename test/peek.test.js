const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test peek() when stack is empty', () => {
  expect(stack.peek()).toThrow(Error);
});

test('Test peek() when just pushed an element', () => {
  stack.push('test');
  expect(stack.peek()).toBe('test');
});

test('Test peek() when stack got pushed then popped', () => {
  stack.push('test1');
  expect(stack.peek()).toBe('test1');
  stack.push('test2');
  expect(stack.peek()).toBe('test2');
  stack.pop();
  expect(stack.peek()).toBe('test1');
});
