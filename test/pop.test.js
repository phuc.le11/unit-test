const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test pop() when stack is empty', () => {
  expect(stack.pop()).toThrow(Error);
});

test('Test pop() consecutive', () => {
  for (var i = 0; i < 5; i++) {
    stack.push(i);
  }
  for (i = 4; i >= 0; i--) {
    expect(stack.pop()).toBe(i);
  }

  expect(stack.pop()).toThrow(Error);
});
