const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test push() return the element that is just pushed', () => {
  expect(stack.push('test')).toBe('test');
});

test('Test push() return after pop()', () => {
  stack.push('test1');
  stack.push('test2');
  stack.pop();

  expect(stack.peek()).toBe('test1');
});

test('Test push() return after clear()', () => {
  stack.push('test1');
  stack.push('test2');
  stack.clear();
  expect(stack.push('test')).toBe('test');
});
