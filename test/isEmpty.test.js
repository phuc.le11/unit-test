const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test isEmpty() when stack is not empty', () => {
  stack.push('test');
  expect(stack.isEmpty()).toBe(false);
});

test('Test isEmpty() when stack is empty', () => {
  expect(stack.isEmpty()).toBe(true);
});

test('Test isEmpty() when stack has 0 then 1 element', () => {
  expect(stack.isEmpty()).toBe(true);
  stack.push('test');
  expect(stack.isEmpty()).toBe(false);
});

test('Test isEmpty() when stack has 1 then 0 element', () => {
  stack.push('test');
  expect(stack.isEmpty()).toBe(false);
  stack.pop();
  expect(stack.isEmpty()).toBe(true);
});

test('Test isEmpty() when pop empty stack', () => {
  try {
    stack.pop();
  } catch (e) {}
  expect(stack.isEmpty()).toBe(true);
});
