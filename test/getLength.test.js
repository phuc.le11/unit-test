const stack = require('../stack');

beforeEach(() => {
  stack.clear();
});

test('Test getLength() when stack is not empty', () => {
  for (let i = 0; i < 5; i++) stack.push(i);
  expect(stack.getLength()).toBe(5);
});

test('Test getLength() when stack is empty', () => {
  expect(stack.getLength()).toBe(0);
});

test('Test getLength() when stack is not empty when push then pop', () => {
  for (var i = 0; i < 7; i++) stack.push(i);

  for (var i = 0; i < 5; i++) stack.pop();
  expect(stack.getLength()).toBe(2);
});

test('Test getLength() when pop empty stack', () => {
  try {
    stack.pop();
  } catch (e) {}
  expect(stack.getLength()).toBe(0);
});
